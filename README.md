# DAILY HARVEST SEARCH EXAMPLE

## Instructions

- Run `npm install`

- To run the example: `npm run findProductsByIngredients` followed by a single search term, ex: `banana`. If the search term contains multiple words seperated by whitespace, wrap the tem in quatation marks, ex: `npm run findProductsByIngredients "Organic Banana"`.

- To test the example: `npm test`

## Functionality

The `npm` script `findProductsByIngredients` evaluates the arguments passed to the function call and exit with a message for the user if the number of aruments is not one (1). If only one argument is passed, the service is called with that argument. The service then loads the `ingredients.json` and searches it for matches. Casing conflicts between lower and uppercase strings are handled, as are partial matches; a search for 'banana' will result in a match for 'Banana' and 'Organic Banana' while a search for 'organic banana' witll return only 'Organic Banana'. If there are no matches to the user's search term, the user is notified in the console. If there is at least one match, The service then loads the `product.json` and compares each product for matches to any of the ingrediet ids provided. If no products contain the ingredients provided by the user, the user is notified in the console. If matching products are found, they are listed in the console.

## Architecture

- The entry point to the example collates and exposes the API and includes rudimentary validation logic.
- The `./product/index.js` file contains business logic related to managing and interacting with products. Constant files for strings and color escapes used in the product file in order to minimize visual noise and clutter.
- Testing was done using Mocha, Chai, and Sinon. Sinon was included to supress comnsole logs during testing

## Improvements

- Add caching/memoization for `product` and `ingedients`. This is a nonpersistent service with a tiny dataset, but persistent or not, an active, user-facing service with a larger dataset would not scale.
- Add handling for input to be a string of Comma-Seperated Values
- Add handling for user to add a second argument listing their food allergies

## In conclusion

    "Thanks, everyone! this was a fun little exercise. I hope you find the results satisfactory, and I look forward to meeting with the team!"
