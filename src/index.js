const COLOR = require("./constant/colorEscapeSequence");
const STRING = require("./constant/string");
const product = require("./product"); // Go here to see the product business logic

module.exports = {
  findProductsByIngredientsService: () => {
    if (process.argv.length === 2) { 
       // The user provided the correct number of arguments, continue
      const response = product.findByIngredients(process.argv[1]);
      const { length } = response.results;

      console.log(COLOR.MESSAGE, response.message);
      console.log(`${length} product${length !== 1 ? "s" : ""}`);
      response.results.map((result, index) => {
        console.log(`${index + 1}. ${result.name} ${result.collection}`);
      });
    } else if (process.argv.length < 2) {
      // The user provided too few arguments, notify them
      console.log(COLOR.WARNING, STRING.ARGUMENT_COUNT_MISMATCH_TOO_FEW);
    } else if (process.argv.length < 2) {
      // The user provided too many arguments, notify them
      console.log(COLOR.WARNING, STRING.ARGUMENT_COUNT_MISMATCH_TOO_MANY);
    }
  }
};
