const fs = require("fs");
const path = require("path");
const STRING = require("../constant/string");

/**
 * @method findByIngredients
 *
 * @description An external method to collate a list of Daily Harvest products containing ingredients provided by a user.
 * @param {string} input the search term inpout by the user
 * @public
 * @returns {object} A response object containing the status of the call to the service and either: 1) a list of products
 *  matching the input search term, or 2) an empty array
 */
function findByIngredients(input) {
  const ingredients = getIngredientsByName(input);
  if (!ingredients.length) {
    return {
      message: STRING.INGREDIENT_DOES_NOT_EXIST(input),
      results: [],
      success: false
    };
  }
  const products = getProductsByIngredients(ingredients);
  if (!products.length) {
    return {
      success: false,
      message: STRING.PRODUCTS_DO_NOT_EXIST,
      results: []
    };
  }
  return {
    message: STRING.PRODUCTS_FOUND,
    results: products,
    success: true
  };
}

/**
 * @method getIngredientsByName
 *
 * @description An internal method to retrieve a list of ingredient objects based on their name properties
 * @param {string} name
 * @private
 * @returns {array} A list of ingredient objects
 */
function getIngredientsByName(name) {
  let ingredientsManifest = parseJson("ingredients");
  return ingredientsManifest.ingredients.filter(ingredient =>
    ingredient.name.toLowerCase().match(name.toLowerCase())
  );
}

/**
 *  @method getProductsByIngredients
 *
 * @description An internal method to retrieve a list of product objects based on their inclusion of ingredients
 * @param {array} A list of ingredient objects
 * @private
 * @returns {array} a list of product objects
 */
function getProductsByIngredients(ingredients) {
  /* 
    TODO This method is computationally expesnsive and in a production app should incorporate either 
    optimized data structures (maps > lists) or caching/memoization or both.
   */
  const searchIngredientIds = ingredients.map(ingredient => ingredient.id);
  let productManifest = parseJson("products");
  return productManifest.products.filter(product =>
    product.ingredient_ids.some(productIngredientId =>
      searchIngredientIds.includes(productIngredientId)
    )
  );
}

/**
 *  @method parseJson
 *
 * @description An internal method to locate a JSON file and parse its data
 * @param {string} target
 * @private
 * @returns {object} the JSON file's data
 */
function parseJson(target) {
  const filePath = path.resolve(__dirname, `../../json/${target}.json`);
  const fileData = fs.readFileSync(filePath);
  return JSON.parse(fileData);
}

module.exports = {
  findByIngredients
};
