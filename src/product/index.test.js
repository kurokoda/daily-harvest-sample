const expect = require("chai").expect;
const sinon = require("sinon");
const product = require("./index.js");

sinon.stub(console, "log");

describe("Product", function() {
  describe("#findByIngredients()", function() {
    it("should return an array of one object when the search term is 'Organic Cherry'", function() {
      const results = product.findByIngredients("Organic Cherry").results
        .length;
      const expected = 1;
      expect(expected).to.eql(results);
    });

    it("should return an array of four objects when the search term is 'Organic Banana'", function() {
      const results = product.findByIngredients("Organic Banana").results
        .length;
      const expected = 4;
      expect(expected).to.eql(results);
    });

    it("should return an array of four objects when the search term is 'Banana'", function() {
      const results = product.findByIngredients("Banana").results.length;
      const expected = 4;
      expect(expected).to.eql(results);
    });

    it("should return an array of four objects when the search term is 'banana' (lowercase)", function() {
      const results = product.findByIngredients("banana").results.length;
      const expected = 4;
      expect(expected).to.eql(results);
    });

    it("should return an array of four objects when the search term is 'ana' (lowercase)", function() {
      const results = product.findByIngredients("ana").results.length;
      const expected = 4;
      expect(expected).to.eql(results);
    });

    it("should return an array of five objects when the search term is 'an' (lowercase)", function() {
      const results = product.findByIngredients("an").results.length;
      const expected = 5;
      expect(expected).to.eql(results);
    });

    it("should return an empty array when the search term is 'z'", function() {
      const results = product.findByIngredients("z").results.length;
      const expected = 0;
      expect(expected).to.eql(results);
    });
  });
});
