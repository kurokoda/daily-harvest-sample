const colorEscapeSequences = {
  ERROR: "\x1b[31m%s\x1b[0m",
  MESSAGE: "\x1b[32m%s\x1b[0m",
  WARNING: "\x1b[33m%s\x1b[0m"
};

module.exports = colorEscapeSequences;
