const strings = {
  ARGUMENT_COUNT_MISMATCH_TOO_FEW:
    "ERROR: This function requires a single argument, a comma separated list of strings",
  ARGUMENT_COUNT_MISMATCH_TOO_MANY:
    "ERROR: This function only accepts a single argument, a comma separated list of strings",
  INGREDIENT_DOES_NOT_EXIST: ingredient =>
    `Sorry, The ingredient you are searching for, ${ingredient} does not exist`,
  MULTIPLE_ARGUMENTS:
    "ERROR: This function only accepts a single argument, a comma separated list of strings",
  NON_STRING_ARGUMENT:
    "ERROR: This function only accepts a comma separated list of strings",
  PRODUCTS_DO_NOT_EXIST:
    "Sorry, There are no products that include the ingredients you are searching for",
  PRODUCTS_FOUND: "Here are your search results"
};

module.exports = strings;
